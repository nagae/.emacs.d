;; ========================================
;;
;; ELPA
;;
;; ========================================
;; ----------------------------------------
;; ELPA (Emacs Lisp Package Archive) の設定
;; ----------------------------------------
(require 'package)
; デフォルトのフォルダを Dropbox 上に
(setq package-user-dir "~/Dropbox/.emacs.d/elpa")
; marmalade リポジトリを追加
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)
