;; ========================================
;;
;; org-mode の基本設定
;;
;; ========================================
;; --------------------------------------------------------------------------------
;; org-mode を呼ぶ前に必要な処理(ここから)
;; ----------------------------------------
; org-mode では auto-fill-mode を切る
(add-hook 'org-mode-hook
          '(lambda ()
             (setq fill-column 80)
             (auto-fill-mode -1)
	     (flyspell-mode t)
             ))

;; org-mode では ispell-parser を tex モードに
(add-hook 'org-mode-hook (lambda () (setq ispell-parser 'tex)))

;; C-c C-j でのインクリメンタルサーチを無効に(org-modeを呼ぶ前に)
(setq org-goto-auto-isearch nil)

;; 強調
(defface my/org-alert-face
        '((t (:weight bold :foreground "black" :foreground "#FF0000")))
        "Face used to display alert'ed items.")
(setq org-emphasis-alist
      '(("*" bold "<b>" "</b>")
	("/" italic "<i>" "</i>")
	("_" underline "<span style=\"text-decoration:underline;\">" "</span>")
	("=" org-code "<code>" "</code>" verbatim)
	("~" org-verbatim "<code>" "</code>" verbatim)
	("@" my/org-alert-face "<span class=\"alert\">" "</span>")
	("+" (:strike-through nil) "<del>" "</del>")))

;(setq org-emphasis-regexp-components 
;      '(" 	('\"{，．" "- 	.,:!?;'\")}\\，．" " 	\n,\"'" "." 1))
(custom-set-variables
 '(org-emphasis-regexp-components (quote (" 	('\"{，．" "- 	.,:!?;'\")}\\，．" " 	
,\"'" "." 1)))
 )


;; ----------------------------------------
;; org-mode を呼ぶ前に必要な処理(ここまで)
;; --------------------------------------------------------------------------------
(require 'org)


;; ----------------------------------------
;; org-mode の設定
;; ----------------------------------------
;; 基本ディレクトリ
;; Set to the location of your Org files on your local system
(setq org-directory "~/Dropbox/org")
;; Set to the name of the file where new notes will be stored
(setq org-mobile-inbox-for-pull "~/Dropbox/org/flagged.org")

;; キーバインドの設定
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)

;; デフォルトの設定
(setq org-startup-folded nil) 		; 折り畳まない
(setq org-startup-indented t)		; インデントする

;; 拡張子がorgのファイルを開いた時，自動的にorg-modeにする
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))

;; org-modeでの強調表示を可能にする
(add-hook 'org-mode-hook 'turn-on-font-lock)

;; 見出しの余分な*を消す
(setq org-hide-leading-stars t)

;; リスト番号にアルファベットも含める
(setq org-alphabetical-lists t)

;; M-x toggle-truncation を実行する度に折り返しを切り替える
;; http://d.hatena.ne.jp/stakizawa/20091025/t1
(setq org-startup-truncated t)
(defun toggle-truncation()
  (interactive)
  (cond ((eq truncate-lines nil)
         (setq truncate-lines t))
        (t
         (setq truncate-lines nil))))
(global-set-key (kbd "C-|") 'toggle-truncate-lines)  ; 折り返し表示ON/OFF

;; ----------------------------------------
;; org-mode + reftex
;; ----------------------------------------
;; RefTeXで使用するbibファイルの位置を指定する
(setq reftex-default-bibliography
      '("~/Dropbox/texmf/bibtex/bib/Mendeley/library.bib"
	"~/Dropbox/texmf/bibtex/bib/mybib.bib"	))

;; org-mode でreftexが使えるようにキーバインドを設定
(defun org-mode-reftex-setup ()
  (load-library "reftex")
  (and (buffer-file-name)
       (file-exists-p (buffer-file-name))
       (reftex-parse-all))
  (define-key org-mode-map (kbd "C-c C-x [") 'reftex-citation)
  (define-key org-mode-map (kbd "C-c C-x =") 'reftex-toc)
  (define-key org-mode-map (kbd "C-c C-x <") 'reftex-index)
  (define-key org-mode-map (kbd "C-c C-x >") 'reftex-display-index)
  (define-key org-mode-map (kbd "C-c C-x &") 'reftex-view-crossref)
  (define-key org-mode-map (kbd "C-c C-x )") 'reftex-reference)
  (define-key org-mode-map (kbd "C-c C-x (") 'reftex-label)
  )
(add-hook 'org-mode-hook 'org-mode-reftex-setup)

;; ----------------------------------------
;; org-mode + LaTeX
;; ----------------------------------------
;; LaTeX文を処理
(setq org-export-with-LaTeX-fragments t)
;; dvipng を使う
;(setq org-latex-create-formula-image-program 'dvipng)
(setq org-latex-create-formula-image-program 'imagemagick)
;; 数式の表示形式
(setq org-format-latex-options 
      (quote
       (:foreground default :background default :scale 1.5 :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers
	     ("begin" "$1" "$" "$$" "\\(" "\\["))))

;; source code block
(add-to-list 'org-structure-template-alist '("AL" "#+ATTR_LaTeX:"))
(add-to-list 'org-structure-template-alist '("AH" "#+ATTR_HTML:"))
(add-to-list 'org-structure-template-alist '("OT" "%BEGIN RECEIVE ORGTBL ?tablename\n\n%END RECEIVE ORGTBL tablename\n\n#+ORGTBL: SEND tablename orgtbl-to-latex :splice t\n"))

;; ----------------------------------------
;; Org-export-latex
;; ----------------------------------------
(require 'org-latex)
(require 'org-special-blocks)

;; PDF で出力するのに latexmk を使う
;(setq org-latex-pdf-process
;      '("latexmk -e '$pdflatex=q/lualatex %S/' -e '$bibtex=q/bibtexu %B/' -e '$biber=q/biber --bblencoding=utf8 -u -U --output_safechars %B/' -e '$makeindex=q/makeindex -o %D %S/' -norc -gg -pdf %f"))
(setq org-latex-pdf-process
      '("lualatex -interaction nonstopmode -output-directory %o %f" "lualatex -interaction nonstopmode -output-directory %o %f" "lualatex -interaction nonstopmode -output-directory %o %f"))


;; 強調や斜体表示をカスタマイズ
(setq org-export-latex-emphasis-alist
      '(("*" "\\textbf{\\gtfamily %s}" nil)
	("/" "\\emph{%s}" nil)
	("@" "\\alert{%s}" nil)
	("_" "\\underline{%s}" nil)
	("+" "\\sout{%s}" nil)
	("=" "\\protectedtexttt" t)
	("~" "\\verb" t)))

;; LaTeX でエクスポート
;; http://d.hatena.ne.jp/tamura70/20100217/org

(setq org-export-latex-coding-system 'utf-8)
(setq org-export-latex-date-format "%Y-%m-%d")
(setq org-export-latex-default-class "esarticle")
(unless (boundp 'org-export-latex-classes)
  (setq org-export-latex-classes nil))
(setq org-export-latex-classes
      '(("jsarticle"
	 "\\documentclass[11pt,a4paper,uplatex]{jsarticle}"
	 ("\\section{%s}" . "\\section*{%s}")
	 ("\\subsection{%s}" . "\\subsection*{%s}")
	 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	 ("\\paragraph{%s}" . "\\paragraph*{%s}")
	 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
	("plain-jsarticle"
	 "\\documentclass[11pt,a4paper,uplatex]{jsarticle}
	 [NO-DEFAULT-PACKAGES]"
	 ("\\section{%s}" . "\\section*{%s}")
	 ("\\subsection{%s}" . "\\subsection*{%s}")
	 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	 ("\\paragraph{%s}" . "\\paragraph*{%s}")
	 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
	("esarticle"
	 "\\documentclass[11pt,a4paper,english,uplatex]{jsarticle}"
	 ("\\section{%s}" . "\\section*{%s}")
	 ("\\subsection{%s}" . "\\subsection*{%s}")
	 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	 ("\\paragraph{%s}" . "\\paragraph*{%s}")
	 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
	("jsbook"
	 "\\documentclass[11pt,a4paper]{jsbook}"
	 ("\\chapter{%s}" . "\\chapter*{%s}")
	 ("\\section{%s}" . "\\section*{%s}")
	 ("\\subsection{%s}" . "\\subsection*{%s}")
	 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	 ("\\paragraph{%s}" . "\\paragraph*{%s}")
	 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
	("beamer"
	 "\\documentclass[t,brown,hyperref={dvipdfmx,setpagesize=false,bookmarks=true,bookmarksnumbered=true,bookmarkstype=toc},color={dvipdfmx}]{beamer}
	 [NO-DEFAULT-PACKAGES]"
	 org-beamer-sectioning)
	("lt-beamer"
	 "\\documentclass{beamer}
          \\usepackage{luatexja}           %Beamerで日本語を使う
         [NO-DEFAULT-PACKAGES]"
	 org-beamer-sectioning)
	("orgwp"
	 "\\documentclass[11pt,a4paper]{jsarticle}
         [DEFAULT-PACKAGES]\\usepackage{orgwp}"
	 ("\\section{%s}" . "\\section*{%s}")
	 ("\\subsection{%s}" . "\\subsection*{%s}")
	 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	 ("\\paragraph{%s}" . "\\paragraph*{%s}")
	 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
	("ltjsarticle"
	 "\\documentclass[11pt,a4paper]{ltjsarticle}\\usepackage{etex}
         [DEFAULT-PACKAGES]"
	 ("\\section{%s}" . "\\section*{%s}")
	 ("\\subsection{%s}" . "\\subsection*{%s}")
	 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	 ("\\paragraph{%s}" . "\\paragraph*{%s}")
	 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
	("amsart"
	 "\\documentclass[11pt,a4paper]{amsart}"
	 ("\\section{%s}" . "\\section*{%s}")
	 ("\\subsection{%s}" . "\\subsection*{%s}")
	 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	 ("\\paragraph{%s}" . "\\paragraph*{%s}")
	 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
	))

;; デフォルトで読み込まれるパッケージ
;; http://tmishina.cocolog-nifty.com/blog/2010/11/org-jsarticle-l.html
(setq org-export-latex-default-packages-alist
  '(;("AUTO"     "inputenc"  t)
    ;("T1"       "fontenc"   t)
    ;; ("dvipdfmx" "graphicx"  t)
    ;; ("dvipdfmx,usenames" "color" nil)
    ;; ("dvipdfmx,setpagesize=false,bookmarks=true,bookmarksnumbered=true,bookmarkstype=toc"     "hyperref"  nil)
    (""         "longtable" nil)
    (""         "float"     nil)
    (""         "latexsym"  t)
    (""         "amssymb"   t)
    (""         "ascmac"   t)
    (""         "fancybox"   t)
    (""         "soul"   t)
    (""         "ulem"   t)
    (""         "ulinej"   t)
    ;; ("expert,deluxe" "otf"  nil)
    ))
;; background で処理しない
(setq org-export-run-in-background nil)


;; ----------------------------------------
;; org-babel
;; ----------------------------------------
(require 'org-exp-blocks)
(org-babel-do-load-languages
 (quote org-babel-load-languages)
 (quote ((emacs-lisp . t)
         (ditaa . t)
         (R . t)
         (python . t)
         (ruby . t)
         (gnuplot . t)
         (clojure . t)
         (sh . t)
         (ledger . t)
         (org . t)
         (plantuml . t)
	 (octave . t)
	 (dot .t)
         (latex . t))))

