;; ========================================
;; 
;; Emacs の基本設定
;; 
;; ========================================

;; ----------------------------------------
;; 日本語関係
;; ----------------------------------------

;; 言語を日本語にする
(set-language-environment 'Japanese)
;; 極力 UTF-8とする
(prefer-coding-system 'utf-8)

;; ----------------------------------------
;; デフォルト設定
;; ----------------------------------------
;; スタートアップメッセージを非表示
(setq inhibit-startup-message t)

;; バッファ末尾に余計な改行コードを防ぐための設定
(setq next-line-add-newlines nil)

;; 列数表示
(column-number-mode 1)

;; 行数表示
(line-number-mode t)

;; 選択部分のハイライト
(transient-mark-mode t)

;; リージョン内の文字をBSで削除
(delete-selection-mode 1)

;; 対応するカッコをハイライト
(show-paren-mode 1)

;; *~ バックアップファイルは必要
(setq make-backup-files t)
;; #* バックアップファイルは必要
(setq auto-save-default t)

;; 補完機能を使う
(setq partial-completion-mode 1) 

;; file名の補完で大文字小文字を区別しない
(setq completion-ignore-case t) 

;; 削除ファイルをゴミ箱へ
(setq delete-by-moving-to-trash t)
(setq trash-directory "~/.Trash")

;; 時刻の表示( 曜日 月 日 時間:分 )
(setq display-time-day-and-date t)
(setq display-time-24hr-format t)
(display-time-mode t)

;; C-x n / C-x C-n の設定
(put 'narrow-to-region 'disabled nil)	; C-x n n でナローイング
(put 'set-goal-column 'disabled nil)	; C-x C-n で goal-column

;; ----------------------------------------
;; キーバインド
;; ----------------------------------------
;; Command-Key and Option-Key
(setq ns-command-modifier (quote meta))	; command key をメタキーに
(setq ns-alternate-modifier (quote super)) ; option key をsuperキーに

;; システムへ修飾キーを渡さない
(setq mac-pass-control-to-system nil)
(setq mac-pass-command-to-system nil)
(setq mac-pass-option-to-system nil)

;; C-k で改行も含めてカット
(setq kill-whole-line t)

;; M-g で goto-line
(global-set-key "\M-g" 'goto-line)

;; M-n, M-p で next-error, previous-error (org のspan tree の検索で利用) 
(global-set-key "\M-n" 'next-error)
(global-set-key "\M-p" 'previous-error)

;; C-? で C-o と同様にウィンドウを切り替え
(global-set-key (kbd "C-:") 'other-window)

;; バッファを C-> C-< で切り替える
(global-set-key (kbd "C-<") 'bs-cycle-previous)
(global-set-key (kbd "C->") 'bs-cycle-next)

;; ediff で別ウィンドウを表示しない
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;; ----------------------------------------
;; dired
;; ----------------------------------------
;; dired で r キーでファイル名変更が行えるように
(require 'wdired)
(define-key dired-mode-map "r" 'wdired-change-to-wdired-mode)

;; dired を拡張
(load "dired-x")

;; dired-filder-mode を dired-modeでonにする
(defun dired-mode-hooks()
	(dired-filter-mode))
(add-hook 'dired-mode-hook 'dired-mode-hooks)

;; dired で RET キーで新規にバッファを作成していたのを a に割当
(put 'dired-find-alternate-file 'disabled nil)
(define-key dired-mode-map (kbd "RET") 'dired-find-alternate-file)
(define-key dired-mode-map "a" 'dired-advertised-find-file)

;;; dired を使って、一気にファイルの coding system (漢字) を変換する
;; http://www.bookshelf.jp/soft/meadow_25.html#SEC278
(require 'dired-aux)
(add-hook 'dired-mode-hook
          (lambda ()
            (define-key (current-local-map) "T"
              'dired-do-convert-coding-system)))

(defvar dired-default-file-coding-system nil
  "*Default coding system for converting file (s).")

(defvar dired-file-coding-system 'no-conversion)

(defun dired-convert-coding-system ()
  (let ((file (dired-get-filename))
        (coding-system-for-write dired-file-coding-system)
        failure)
    (condition-case err
        (with-temp-buffer
          (insert-file file)
          (write-region (point-min) (point-max) file))
      (error (setq failure err)))
    (if (not failure)
        nil
      (dired-log "convert coding system error for %s:\n%s\n" file failure)
      (dired-make-relative file))))

(defun dired-do-convert-coding-system (coding-system &optional arg)
  "Convert file (s) in specified coding system."
  (interactive
   (list (let ((default (or dired-default-file-coding-system
                            buffer-file-coding-system)))
           (read-coding-system
            (format "Coding system for converting file (s) (default, %s): "
                    default)
            default))
         current-prefix-arg))
  (check-coding-system coding-system)
  (setq dired-file-coding-system coding-system)
  (dired-map-over-marks-check
   (function dired-convert-coding-system) arg 'convert-coding-system t))

;; ----------------------------------------
;; eshell
;; ----------------------------------------
;; s-! で eshell-command を起動できるように
(global-set-key (kbd "s-!") 'eshell-command)
(setq eshell-directory-name "~/Dropbox/.emacs.d/eshell/")
;(add-hook 'after-init-hook (lambda () (eshell)))

;; ----------------------------------------
;; auto-fill-mode
;; ----------------------------------------
;; テキスト・モードでは auto-fill-mode
(add-hook 'text-mode-hook
          '(lambda ()
             (setq fill-column 80)
             (auto-fill-mode t)
             ))

;; ----------------------------------------
;; magit
;; ----------------------------------------
(setq magit-last-seen-setup-instructions "1.4.0") ; 1.4.0のメッセージを表示させない

;; ----------------------------------------
;; aspell
;; ----------------------------------------
(setq-default ispell-program-name "aspell")
(with-eval-after-load "ispell"
  (add-to-list 'ispell-skip-region-alist '("[^\000-\377]+")))

;; ----------------------------------------
;; c-mode
;; ----------------------------------------
;; c-mode でのコメントを // にする
(add-hook 'c-mode-hook (lambda () (setq comment-start "//"
                                        comment-end   "")))

