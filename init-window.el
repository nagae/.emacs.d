;; ========================================
;;
;; ウィンドウ関連
;;
;; ========================================
;; スクロールバーを非表示
(set-scroll-bar-mode nil)

;; ツールバーを非表示
(tool-bar-mode -1)

;; ドラッグ&ドロップの挙動：挿入しないで新たにファイルを開く
(define-key global-map [ns-drag-file] 'ns-find-file)

;; 新しいウィンドウを開かずに，新規バッファに開く
(setq ns-pop-up-frames nil)


;; ----------------------------------------
;; Emacs 24.3 でのフルスクリーン切り替え
;; ----------------------------------------
;; M-x toggle-fullscreen でフルスクリーンの切り替え
(defun toggle-fullscreen ()
  (interactive)
  (set-frame-parameter nil 'fullscreen (if (frame-parameter nil 'fullscreen)
                                           nil
                                           'fullboth)))
;; C-^ にフルスクリーン切り替えを割当てる
(global-set-key (kbd "C-^") 'toggle-fullscreen)

;; ----------------------------------------
;; フォントの設定 
;; ----------------------------------------
;; (create-fontset-from-ascii-font 
;;  "Menlo-12:weight=normal:slant=normal" nil "menlokakugo")
;; (set-fontset-font "fontset-menlokakugo"
;;                   'unicode
;;                   (font-spec :family "Hiragino Kaku Gothic ProN" :height 120)
;;                   nil
;;                   'append)
;; (setq default-frame-alist nil)
;; (add-to-list 'default-frame-alist '(font . "fontset-menlokakugo"))

(when (>= emacs-major-version 24)
  ;; フォントセットを作る
  (let* ((fontset-name "myfonts") ; フォントセットの名前
         (size 12) ; ASCIIフォントのサイズ [9/10/12/14/15/17/19/20/...]
         (asciifont "Menlo") ; ASCIIフォント
         (jpfont "Hiragino Kaku Gothic ProN") ; 日本語フォント
         (font (format "%s-%d:weight=normal:slant=normal" asciifont size))
         (fontspec (font-spec :family asciifont))
         (jp-fontspec (font-spec :family jpfont))
         (fsn (create-fontset-from-ascii-font font nil fontset-name)))
    (set-fontset-font fsn 'japanese-jisx0213.2004-1 jp-fontspec)
    (set-fontset-font fsn 'japanese-jisx0213-2 jp-fontspec)
    (set-fontset-font fsn 'katakana-jisx0201 jp-fontspec) ; 半角カナ
    (set-fontset-font fsn '(#x0080 . #x024F) fontspec)    ; 分音符付きラテン
    (set-fontset-font fsn '(#x0370 . #x03FF) fontspec)    ; ギリシャ文字
    )
 
  ;; デフォルトのフレームパラメータでフォントセットを指定
  (add-to-list 'default-frame-alist '(font . "fontset-myfonts"))
 
  ;; フォントサイズの比を設定
  ;; 半角と全角の比を1:2にしたければ
  (setq face-font-rescale-alist
	;;        '((".*Hiragino_Mincho_pro.*" . 1.2)))
	'((".*Hiragino_Kaku_Gothic_ProN.*" . 1.2)));; Mac用フォント設定
 
  ;; デフォルトフェイスにフォントセットを設定
  ;; # これは起動時に default-frame-alist に従ったフレームが作成されない現象への対処
  (set-face-font 'default "fontset-myfonts"))

;; ----------------------------------------
;; テーマの設定
;; ----------------------------------------
;; テーマへのパスを設定
(add-to-list 'custom-theme-load-path "~/Dropbox/.emacs.d/themes/")
;; dark-laptop: https://github.com/lliles/dark-laptop
(load-theme 'wheatgrass t)

;; ----------------------------------------
;; spaces.el
;; ----------------------------------------
;; clone git@github.com:emacsattic/spaces.git
(require 'spaces)
(define-prefix-command 'spaces-map)
(global-set-key (kbd "C-z") 'spaces-map)
(global-set-key (kbd "C-\"") 'sp-switch-space) ; C-,とC-'はorg-mode に取られている
(global-set-key (kbd "C-z n") 'sp-new-space)
(global-set-key (kbd "C-z C-k") 'sp-clear-spaces)
(global-set-key (kbd "C-z k") 'sp-kill-space)
(global-set-key (kbd "C-z C-s") 'sp-save-space)

;; ----------------------------------------
;; ポップアップ・ダイアログを禁止する
;; ----------------------------------------
(defadvice yes-or-no-p (around prevent-dialog activate)
  "Prevent yes-or-no-p from activating a dialog"
  (let ((use-dialog-box nil))
    ad-do-it))
(defadvice y-or-n-p (around prevent-dialog-yorn activate)
  "Prevent y-or-n-p from activating a dialog"
  (let ((use-dialog-box nil))
    ad-do-it))
