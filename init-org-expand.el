;; ========================================-
;;
;; org-mode の拡張設定
;;
;; ========================================
;; ----------------------------------------
;; org-agenda
;; ----------------------------------------
(setq org-agenda-files (list "~/Dropbox/org/todo.org"))

;; ----------------------------------------
;; MobileOrg
;; ----------------------------------------
;; Set to <your Dropbox root directory>/MobileOrg.
(setq org-mobile-directory "~/Dropbox/MobileOrg")

;; ----------------------------------------
;; org-capture-mode
;; ----------------------------------------
;;(setq org-default-notes-file (concat org-directory "~/Dropbox/org/notes.org"))
(define-key global-map "\C-cc" 'org-capture)

(setq org-capture-templates
      '(("t" "Todo" entry (file+headline "~/Dropbox/org/todo.org" "Tasks")
             "* TODO %?\n %i\n %a")
        ("j" "Journal" entry (file+datetree "~/Dropbox/org/journal.org")
             "* %?\ %U\n %i\n %a")
        ("n" "Note" entry (file+headline "~/Dropbox/org/notes.org" "Notes")
             "* %?\n %U\n %i")
        ("m" "Minutes" entry (file+headline "~/Dropbox/org/minutes.org" "Minutes")
             "* %u %?\n #+title: \n - 日時:%<%Y>年%<%m>月%<%d>日%<%H:%M>\n - 場所:\n - 出席者:\n - 欠席者:")
         ))

;; ----------------------------------------
;; org-publish-mode
;; ----------------------------------------
(require 'org-publish)
(setq org-publish-project-alist
      '(("bitbucket"
	 :base-directory "~/home/_work/nagae.bitbucket.org/"
	 :publishing-directory "~/home/_work/nagae.bitbucket.org/"
	 :recursive t
	 :section-numbers nil
	 )
	))

;; ----------------------------------------
;; org-mode + reftex 用のフォルダを追加
;; ----------------------------------------
;; (require 'org-exp-bibtex)
;; ;; (add-to-list 'reftex-default-bibliography
;; ;; 	     '("~/home/_work/_reference/rmss_minimal.bib"
;; ;; 	       "~/home/_work/texmf/bibtex/bib/jmybib.bib"
;; ;; 	       "~/home/_work/texmf/bibtex/bib/paper.bib"
;; ;; 	       "~/home/_work/texmf/bibtex/bib/work.bib"
;; ;; 	       "~/Dropbox/texmf/bibtex/bib/Mendeley/library.bib"
;; ;; 	       "~/Dropbox/texmf/bibtex/bib/mybib.bib"
;; ;; 	       ))

