;; ========================================
;; 
;; 基本設定
;; 
;; ========================================

;; ----------------------------------------
;; バージョン判定フラグ
;; ----------------------------------------
(defvar is_emacs23 (equal emacs-major-version 23))
(defvar is_emacs24 (equal emacs-major-version 24))
(defvar is_later23 (or is_emacs23 is_emacs24))
(defvar is_window-sys (not (eq (symbol-value 'window-system) nil)))
(defvar is_mac (or (eq window-system 'mac) (featurep 'ns)))
(defvar is_cocoa (and is_mac is_later23 is_window-sys))

;; ----------------------------------------
;; Lisp パッケージの置き場所
;; ----------------------------------------
;; http://www.emacswiki.org/emacs/LoadPath
;; ~/Dropbox/.emacs.d/lisp 以下のフォルダを全て load-path に追加する
(setq default-directory (concat (getenv "HOME") "/"))
(let ((default-directory "~/Dropbox/.emacs.d/lisp/"))
  (add-to-list 'load-path default-directory)
  (normal-top-level-add-subdirs-to-load-path))

;; ----------------------------------------
;; 設定ファイルを読み込む
;; ----------------------------------------
;; Package を初期化する
(setq package-enable-at-startup nil)
(package-initialize)
;; 共通の設定(キーバインドなど)
(load "~/Dropbox/.emacs.d/init-emacs.el")
;; DDSKK(かな漢字入力方式)を使わない場合はコメントアウト
(load "~/Dropbox/.emacs.d/init-ddskk.el")
;; ELPA パッケージ
(load "~/Dropbox/.emacs.d/init-elpa.el" t)
;; 以下の lisp は window-system の場合にのみ読み込む(ターミナルでの起動高速化のため)
(cond (is_window-sys
       ;; Lisp 関連
       (load "~/Dropbox/.emacs.d/init-lisp.el" t)
       ;; ウィンドウ関連
       (load "~/Dropbox/.emacs.d/init-window.el" t)
       ;; org-mode 8
       (load "~/Dropbox/.emacs.d/init-org.el" t)
       ;; C言語
       (load "~/Dropbox/.emacs.d/init-clang.el" t)
       ;; org-mode の基本設定
       ;(load "~/Dropbox/.emacs.d/init-org-basic.el" t)
       ;; org-mode の拡張設定
       ;(load "~/Dropbox/.emacs.d/init-org-expand.el" t)
       ;; TeX 関連 
       (load "~/Dropbox/.emacs.d/init-tex.el" t)
       ;; Gnus 関連 
       (load "~/Dropbox/.emacs.d/init-gnus.el" t)
       ;; Python
       ;(load "~/Dropbox/.emacs.d/init-python.el" t)
       ))
