;; ========================================
;;
;; Lisp Package の設定
;;
;; ========================================


;; ----------------------------------------
;; anything
;; ----------------------------------------
;; M-x package-install RET anything RET
;; option-a に anything-for-files を割り当て
(define-key global-map (kbd "s-a") 'anything-for-files)

;; ----------------------------------------
;; セッションを自動的に保存
;; ----------------------------------------
;; M-x package-install RET session RET
(add-hook 'after-init-hook 'session-initialize)
(setq session-save-file-coding-system 'utf-8-unix)
(setq session-save-file (expand-file-name
			 "~/Dropbox/.emacs.d/.session"))

;; ----------------------------------------
;; browse-kill-ring を
;; ----------------------------------------
;; M-x package-install browse-kill-ring RET
;; M-y を単独で使う場合には browse-kil-ring を呼び出せる
(browse-kill-ring-default-keybindings)

;; ----------------------------------------
;; word-count-mode
;; ----------------------------------------
;; http://www.emacswiki.org/emacs/WordCount
;; ----------------------------------------
;; M-x package-install RET wc-mode RET
(require 'wc-mode)
(global-set-key "\M-+" 'wc-mode)

;; ----------------------------------------
;; sr-speedbar
;; ----------------------------------------
;; http://www.emacswiki.org/emacs/sr-speedbar.el
;; ----------------------------------------
;; M-x package-install RET sr-speedbar RET
(require 'sr-speedbar)
;; s-s で sr-speedbar を呼び出す
(global-set-key (kbd "s-s") 'sr-speedbar-toggle)
;; sr-speedbar ウィンドウで ^ を押すと1つ上のディレクトリに移動
(define-key speedbar-file-key-map "^" 'speedbar-up-directory)

;; ----------------------------------------
;; redo+
;; ----------------------------------------
;; M-x package-install RET redo+ RET
;; (require 'redo+)
;; undo に redo 機能をつけない
;;(setq undo-no-redo t)
;; C-? に redo を割り当て
;;(global-set-key (kbd "C-?") 'redo)

;; ----------------------------------------
;; magit
;; ----------------------------------------
;; http://philjackson.github.com/magit/
;; ----------------------------------------
;; $ brew install magit
(require 'magit)
;; C-x g でmagit-status を呼び出す
(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key [(control \?)] 'magit-status)

;; ----------------------------------------
;; octave-mode
;; ----------------------------------------
;; http://www.gnu.org/software/octave/doc/interpreter/Using-Octave-Mode.html
(autoload 'octave-mode "octave-mod" nil t)
(add-hook 'octave-mode-hook
          (lambda ()
            (abbrev-mode 1)
            (auto-fill-mode 1)
            (if (eq window-system 'x)
                (font-lock-mode 1))))
(setq auto-mode-alist
      (cons '("\\.m$" . octave-mode) auto-mode-alist))
(autoload 'run-octave "octave-inf" nil t)

;; ----------------------------------------
;; markdown-mode
;; ----------------------------------------
;; http://jblevins.org/projects/markdown-mode/
(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.md" . gfm-mode))

;; SKKで変換確定を return で行なうと行頭にカーソルが飛ぶ問題の対策
(defun my--markdown-entery-key-ad (this-func &rest args)
  "markdown-modeでskk-henkan-mode中にエンターすると行頭にカーソルが飛んでしまう問題の対応"
  (if skk-henkan-mode (skk-kakutei)
    (apply this-func args)))
(advice-add #'markdown-enter-key :around #'my--markdown-entery-key-ad)

